﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
// Data binding
using OurSweetLove.Model;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace OurSweetLove
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        // Định nghĩa 2 list
        private List<MenuItem> MenuItems;
        private List<TopOption> TopOptions;

        public MainPage()
        {
            this.InitializeComponent();
            // Sử dụng phương thức
            MenuItems = MenuItemManager.AddItems();
            TopOptions = TopOptionManager.AddOption();
        }

        // Overide thiết lập mặc định ...
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            MainFrame.Navigate(typeof(HomePage));
        }

        private void Hamburger_Click(object sender, RoutedEventArgs e)
        {
                Split.IsPaneOpen = !Split.IsPaneOpen;
        }

        private void MenuList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var item = e.ClickedItem as MenuItem;
            Title.Text = item.ItemName;

            switch (item.ItemName)
            {
                case "Your Love":
                    MainFrame.Navigate(typeof(HomePage));   
                    break;
                case "Security":
                    MainFrame.Navigate(typeof(SecurityPage));
                    break;
                case "Share":
                    MainFrame.Navigate(typeof(SharePage));
                    break;
                case "Rate The App":
                    break;
                case "Personalize":
                    MainFrame.Navigate(typeof(PersonalizePage));
                    break;
                case "About":
                    MainFrame.Navigate(typeof(AboutPage));
                    break;
                default:
                    break;
            }
            
        }

        private void OptionList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var option = e.ClickedItem as TopOption;

            switch (option.OptionName)
            {
                case "Edit":
                    MainFrame.Navigate(typeof(EditPage));
                    break;
                case "Delete":
                    // Xoá tất cả dữ liệu trừ cài đặt app
                    break;
            }
        }
    }
}
